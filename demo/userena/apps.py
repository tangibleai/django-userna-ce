from django.apps import AppConfig


class UserenaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = "userena"
    verbose_name = "Userena"
