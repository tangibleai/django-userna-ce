#!/usr/bin/env bash

# Exit on error
set -o errexit

# Open demo 
cd demo

# Install dependencies
pip install -r requirements.txt

# Serve static files
python manage.py collectstatic --no-input

# Make and apply migrations
python manage.py makemigrations 
python manage.py migrate
